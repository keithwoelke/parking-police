resource "google_storage_bucket" "incoming_sightings_cloud_function" {
  name                        = "${var.project_name}-${var.function_name}-cloud-function"
  location                    = var.region
  uniform_bucket_level_access = true

  lifecycle_rule {
    action {
      type = "Delete"
    }

    condition {
      age = 1
    }
  }
}

data "archive_file" "zipped_incoming_sightings_cloud_function" {
  type        = "zip"
  source_dir  = "${local.root_dir}/cmd/${var.function_name}-cloud-function"
  output_path = "${local.root_dir}/cmd/${var.function_name}-cloud-function/${var.function_name}-cloud-function.zip"
  excludes    = ["**/*.zip"]
}

resource "google_storage_bucket_object" "zipped_incoming_sightings_cloud_function" {
  name   = "${var.function_name}.zip#${data.archive_file.zipped_incoming_sightings_cloud_function.output_md5}"
  bucket = google_storage_bucket.incoming_sightings_cloud_function.id
  source = data.archive_file.zipped_incoming_sightings_cloud_function.output_path
}

resource "google_project_service" "cloud_functions" {
  project = var.project_name
  service = "cloudfunctions.googleapis.com"

  disable_dependent_services = true
}

resource "google_project_service" "cloud_build" {
  project = var.project_name
  service = "cloudbuild.googleapis.com"

  disable_dependent_services = true
}

resource "google_cloudfunctions_function" "incoming-sightings" {
  name    = var.function_name
  runtime = "go116"

  available_memory_mb   = 128
  source_archive_bucket = google_storage_bucket.incoming_sightings_cloud_function.id
  source_archive_object = google_storage_bucket_object.zipped_incoming_sightings_cloud_function.name
  entry_point           = "AddSighting"

  event_trigger {
    event_type = "google.storage.object.finalize"
    resource   = google_storage_bucket.incoming_sightings.id
  }
}

resource "google_storage_bucket" "incoming_sightings" {
  name                        = "${var.project_name}-incoming-sightings"
  location                    = var.region
  force_destroy               = false
  uniform_bucket_level_access = true
  storage_class               = "STANDARD"

  lifecycle_rule {
    action {
      type = "Delete"
    }

    condition {
      age = 7
    }
  }
}
