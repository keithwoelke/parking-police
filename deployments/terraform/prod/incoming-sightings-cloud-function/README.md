# incoming-sightings-cloud-function

This cloud function is responsible for receiving Pub/Sub notifications from the `parking-police-incoming-sightings`
bucket. It will then pull the file, base64 encode it, wrap it in a JSON payload, and forward it to the `/sightings`
endpoint.
