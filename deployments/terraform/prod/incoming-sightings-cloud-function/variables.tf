variable "function_name" {
  type    = string
  default = "incoming-sightings"
}

variable "region" {
  type    = string
  default = "us-west1"
}

variable "project_name" {
  type    = string
  default = "parking-police"
}
