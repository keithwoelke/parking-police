terraform {
  backend "gcs" {
    bucket = "parking-police-tfstate"
    prefix = "incoming-sightings-cloud-function"
  }

  required_providers {
    google = {
      version = "4.18.0"
    }
  }
}

locals {
  root_dir = abspath("../../../../")
}

provider "google" {
  project = var.project_name
  region  = var.region
}
