variable "api_token" {
  type = string
}

variable "region" {
  type    = string
  default = "US-WEST1"
}

variable "project_name" {
  type    = string
  default = "parking-police"
}
