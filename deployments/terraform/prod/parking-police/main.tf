terraform {
  backend "gcs" {
    bucket = "parking-police-tfstate"
    prefix = "parking-police"
  }

  required_providers {
    google = {
      version = "4.18.0"
    }
  }
}

provider "google" {
  project = var.project_name
  region  = var.region
}

resource "google_storage_bucket" "tfstate" {
  name                        = "${var.project_name}-tfstate"
  location                    = var.region
  force_destroy               = false
  uniform_bucket_level_access = true
  storage_class               = "STANDARD"

  versioning {
    enabled = true
  }

  lifecycle_rule {
    action {
      type = "Delete"
    }

    condition {
      age                = 7
      num_newer_versions = 1
    }
  }
}

#resource "google_pubsub_subscription" "incoming_sightings" {
#  name  = "incoming-sightings-subscription"
#  topic = google_pubsub_topic.incoming_sightings.id
#
#  push_config {
#    push_endpoint = "https://${var.project_id}.${lower(var.region)}.r.appspot.com/sighting?api_token=${var.api_token}"
#  }
#}
