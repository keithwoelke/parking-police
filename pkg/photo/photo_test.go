package photo_test

import (
	"bytes"
	"image"
	"image/png"
	"io/ioutil"
	"math"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/keithwoelke/parking-police/pkg/photo"
)

func TestSupportedImageFormats(t *testing.T) {
	var imageFormats = []struct {
		imageFormat string
		imagePath   string
	}{
		{
			imageFormat: "png",
			imagePath:   "testdata/images/formats/2x2.png",
		},
		{
			imageFormat: "bmp",
			imagePath:   "testdata/images/formats/2x2.bmp",
		},
		{
			imageFormat: "gif",
			imagePath:   "testdata/images/formats/2x2.gif",
		},
		{
			imageFormat: "jpeg",
			imagePath:   "testdata/images/formats/2x2.jpg",
		},
		{
			imageFormat: "tiff",
			imagePath:   "testdata/images/formats/2x2.tif",
		},
	}

	for _, tt := range imageFormats {
		tt := tt
		t.Run(tt.imageFormat, func(t *testing.T) {
			t.Parallel()

			imageBytes, err := ioutil.ReadFile(tt.imagePath)
			require.NoError(t, err)

			img, format, err := image.Decode(bytes.NewReader(imageBytes))
			require.NoError(t, err)
			require.NotNil(t, img)
			assert.Equal(t, tt.imageFormat, format)

			imageSize := img.Bounds().Size()
			require.NotNil(t, imageSize)

			croppedImageBytes, err := photo.Crop(imageBytes, 0, 0, imageSize.X, imageSize.Y)
			require.NoError(t, err)
			require.NotNil(t, croppedImageBytes)

			croppedImg, err := png.Decode(bytes.NewReader(croppedImageBytes))
			require.NoError(t, err)
			assert.NotNil(t, croppedImg)
		})
	}
}

func TestImageCropping(t *testing.T) {
	var images = []struct {
		description   string
		imagePath     string
		x             int
		y             int
		width         int
		height        int
		expectedRed   uint32
		expectedGreen uint32
		expectedBlue  uint32
	}{
		{
			description: "crop upper left",
			imagePath:   "testdata/images/formats/2x2.png",
			x:           0,
			y:           0,
			width:       1,
			height:      1,
			expectedRed: math.MaxUint16,
		},
		{
			description:   "crop lower right",
			imagePath:     "testdata/images/formats/2x2.png",
			x:             1,
			y:             1,
			width:         1,
			height:        1,
			expectedRed:   math.MaxUint16,
			expectedGreen: math.MaxUint16,
			expectedBlue:  math.MaxUint16,
		},
	}

	for _, tt := range images {
		tt := tt
		t.Run(tt.description, func(t *testing.T) {
			t.Parallel()

			imageBytes, err := ioutil.ReadFile(tt.imagePath)
			require.NoError(t, err)

			croppedImageBytes, err := photo.Crop(imageBytes, tt.x, tt.y, tt.width, tt.height)
			require.NoError(t, err)
			require.NotNil(t, croppedImageBytes)

			img, _, err := image.Decode(bytes.NewReader(croppedImageBytes))
			require.NoError(t, err)
			require.NotNil(t, img)

			imageSize := img.Bounds().Size()
			require.NotNil(t, imageSize)

			require.Equal(t, 1, imageSize.X)
			require.Equal(t, 1, imageSize.Y)

			color := img.At(0, 0)
			red, green, blue, alpha := color.RGBA()

			assert.Equal(t, tt.expectedRed, red)
			assert.Equal(t, tt.expectedGreen, green)
			assert.Equal(t, tt.expectedBlue, blue)
			assert.Equal(t, uint32(math.MaxUint16), alpha)
		})
	}
}

func TestInvalidCroppingParameters(t *testing.T) {
	var images = []struct {
		description string
		imagePath   string
		x           int
		y           int
		width       int
		height      int
	}{
		{
			description: "out of bounds x",
			imagePath:   "testdata/images/formats/2x2.png",
			x:           2,
			y:           0,
			width:       1,
			height:      1,
		},
		{
			description: "out of bounds y",
			imagePath:   "testdata/images/formats/2x2.png",
			x:           0,
			y:           2,
			width:       1,
			height:      1,
		},
		{
			description: "zero width",
			imagePath:   "testdata/images/formats/2x2.png",
			x:           0,
			y:           0,
			width:       0,
			height:      1,
		},
		{
			description: "zero height",
			imagePath:   "testdata/images/formats/2x2.png",
			x:           0,
			y:           0,
			width:       1,
			height:      0,
		},
		{
			description: "negative x",
			imagePath:   "testdata/images/formats/2x2.png",
			x:           -1,
			y:           0,
			width:       1,
			height:      1,
		},
		{
			description: "negative y",
			imagePath:   "testdata/images/formats/2x2.png",
			x:           0,
			y:           -1,
			width:       1,
			height:      1,
		},
		{
			description: "invalid image",
			imagePath:   "testdata/images/formats/invalid.img",
			x:           0,
			y:           0,
			width:       1,
			height:      1,
		},
		{
			description: "crop with negative width",
			imagePath:   "testdata/images/formats/2x2.png",
			x:           1,
			y:           0,
			width:       -1,
			height:      1,
		},
		{
			description: "crop with negative height",
			imagePath:   "testdata/images/formats/2x2.png",
			x:           0,
			y:           1,
			width:       1,
			height:      -1,
		},
	}

	for _, tt := range images {
		tt := tt
		t.Run(tt.description, func(t *testing.T) {
			t.Parallel()

			imageBytes, err := ioutil.ReadFile(tt.imagePath)
			require.NoError(t, err)

			_, err = photo.Crop(imageBytes, tt.x, tt.y, tt.width, tt.height)
			require.Error(t, err)
		})
	}
}
