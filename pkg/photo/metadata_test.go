package photo_test

import (
	"io/ioutil"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/keithwoelke/parking-police/pkg/photo"
)

func TestGetMetadata(t *testing.T) {
	var imageFormats = []struct {
		description string
		imagePath   string
	}{
		{
			description: "jpeg",
			imagePath:   "testdata/images/exif/2x2.jpg",
		},
		{
			description: "tiff",
			imagePath:   "testdata/images/exif/2x2.tif",
		},
		{
			description: "png",
			imagePath:   "testdata/images/exif/2x2.png",
		},
		{
			description: "missing DateTimeOriginal",
			imagePath:   "testdata/images/exif/missing-date-time-original.png",
		},
	}

	for _, tt := range imageFormats {
		tt := tt
		t.Run(tt.description, func(t *testing.T) {
			t.Parallel()

			imageBytes, err := ioutil.ReadFile(tt.imagePath)
			require.NoError(t, err)

			metadata, err := photo.ImageMetadata(imageBytes)
			require.NoError(t, err)

			assert.Equal(t, time.Date(1970, time.January, 1, 0, 0, 0, 0, time.UTC), *metadata.DateTime)
			assert.Equal(t, 38.8977, metadata.GPSInfo.Latitude)
			assert.Equal(t, -77.0365, metadata.GPSInfo.Longitude)
		})
	}
}

func TestGetMetadataWhenNoExifMetadata(t *testing.T) {
	var imageFormats = []struct {
		imageFormat string
		imagePath   string
	}{
		{
			imageFormat: "bmp",
			imagePath:   "testdata/images/formats/2x2.bmp",
		},
		{
			imageFormat: "gif",
			imagePath:   "testdata/images/formats/2x2.gif",
		},
		{
			imageFormat: "invalid image",
			imagePath:   "testdata/images/formats/invalid.img",
		},
	}

	for _, tt := range imageFormats {
		tt := tt
		t.Run(tt.imageFormat, func(t *testing.T) {
			t.Parallel()

			imageBytes, err := ioutil.ReadFile(tt.imagePath)
			require.NoError(t, err)

			metadata, err := photo.ImageMetadata(imageBytes)
			require.Error(t, err)

			assert.Nil(t, metadata.DateTime)
			assert.Nil(t, metadata.GPSInfo)
		})
	}
}
