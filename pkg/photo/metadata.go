package photo

import (
	"time"

	"github.com/dsoprea/go-exif"
	"github.com/pkg/errors"
)

type GPSInfo struct {
	Latitude  float64
	Longitude float64
}

type Metadata struct {
	DateTime *time.Time
	GPSInfo  *GPSInfo
}

func ImageMetadata(imageBytes []byte) (metadata Metadata, err error) {
	index, err := parseIFDs(imageBytes)
	if err != nil {
		return metadata, errors.WithStack(err)
	}

	gpsInfo, err := imageGPSInfo(index)
	if err == nil {
		metadata.GPSInfo = &gpsInfo
	} else {
		return metadata, errors.WithStack(err)
	}

	dateTime, err := imageDateTime(index)
	if err == nil {
		metadata.DateTime = &dateTime
	} else {
		return metadata, errors.WithStack(err)
	}

	return metadata, nil
}

func parseIFDs(imageBytes []byte) (index exif.IfdIndex, err error) {
	rawExif, err := exif.SearchAndExtractExif(imageBytes)
	if err != nil {
		return index, errors.WithStack(err)
	}

	ifdMapping := exif.NewIfdMapping()

	err = exif.LoadStandardIfds(ifdMapping)
	if err != nil {
		return index, errors.WithStack(err)
	}

	tagIndex := exif.NewTagIndex()

	_, index, err = exif.Collect(ifdMapping, tagIndex, rawExif)
	if err != nil {
		return index, errors.WithStack(err)
	}

	return index, nil
}

func imageDateTime(index exif.IfdIndex) (dateTime time.Time, err error) {
	ifd, err := index.RootIfd.ChildWithIfdPath(exif.IfdPathStandardExif)
	if err != nil {
		return dateTime, errors.WithStack(err)
	}

	tagEntries, err := ifd.FindTagWithName("DateTimeOriginal")
	if err != nil {
		tagEntries, err = ifd.FindTagWithName("DateTimeDigitized")
		if err != nil {
			return dateTime, errors.WithStack(err)
		}
	}

	dateTimeValue, _ := ifd.TagValue(tagEntries[0])

	return time.Parse("2006:01:02 15:04:05", dateTimeValue.(string))
}

func imageGPSInfo(index exif.IfdIndex) (gpsInfo GPSInfo, err error) {
	ifd, err := index.RootIfd.ChildWithIfdPath(exif.IfdPathStandardGps)
	if err != nil {
		return gpsInfo, errors.WithStack(err)
	}

	rawGPSInfo, err := ifd.GpsInfo()
	if err != nil {
		return gpsInfo, errors.WithStack(err)
	}

	gpsInfo = GPSInfo{
		Latitude:  rawGPSInfo.Latitude.Decimal(),
		Longitude: rawGPSInfo.Longitude.Decimal(),
	}

	return gpsInfo, nil
}
