// Package photo contains the methods necessary for manipulation of vehicle and plate photos. Supported formats are png,
// jpeg, gif, bmp, and tiff.
package photo

import (
	"bytes"
	"image"
	_ "image/gif"  // register gif with the imagePath library
	_ "image/jpeg" // register jpeg with the imagePath library
	"image/png"

	"github.com/oliamb/cutter"
	"github.com/pkg/errors"
	_ "golang.org/x/image/bmp"  // register bmp with the imagePath library
	_ "golang.org/x/image/tiff" // register tiff with the imagePath library
)

// Crop an imagePath which from (x,y) to the provided width and height. The resulting imagePath will be returned as a
// []byte.
func Crop(imageBytes []byte, x, y, width, height int) (croppedImageBytes []byte, err error) {
	img, _, err := image.Decode(bytes.NewReader(imageBytes))
	if err != nil {
		return croppedImageBytes, err
	}

	imageBounds := img.Bounds()
	if coordinatesAreWithinImage(x, y, imageBounds) {
		return croppedImageBytes, errors.Errorf("anchor outside of image bounds: %d <= x <= %d, X: %d; %d <= y <= %d, Y: %d", imageBounds.Min.X, imageBounds.Max.X, x, imageBounds.Min.Y, imageBounds.Max.Y, y)
	}

	if heightAndWidthAreWithinImage(x, y, width, height, imageBounds) {
		return croppedImageBytes, errors.Errorf("width and/or height outside of image bounds: 1 <= width <= %d, width: %d; 1 <= y <= %d, Y: %d", imageBounds.Max.X-x, width, imageBounds.Max.Y-y, height)
	}

	var croppedImg image.Image
	croppedImg, err = cutter.Crop(img, cutter.Config{
		Anchor: image.Point{X: x, Y: y},
		Width:  width,
		Height: height,
	})
	if err != nil {
		return croppedImageBytes, errors.WithStack(err)
	}

	croppedImageBytesBuffer := new(bytes.Buffer)
	if err = png.Encode(croppedImageBytesBuffer, croppedImg); err != nil {
		return croppedImageBytes, errors.WithStack(err)
	}

	return croppedImageBytesBuffer.Bytes(), nil
}

func coordinatesAreWithinImage(x, y int, imageBounds image.Rectangle) (withinBounds bool) {
	return x > imageBounds.Max.X || x < imageBounds.Min.X || y > imageBounds.Max.Y || y < imageBounds.Min.Y
}

func heightAndWidthAreWithinImage(x, y, width, height int, imageBounds image.Rectangle) (withinBounds bool) {
	return x+width > imageBounds.Max.X || y+height > imageBounds.Max.Y || width <= 0 || height <= 0
}
