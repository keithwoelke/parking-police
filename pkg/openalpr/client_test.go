package openalpr_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/keithwoelke/parking-police/pkg/openalpr"
)

func TestNewClient(t *testing.T) {
	client, err := openalpr.New("")
	require.NoError(t, err)

	require.NotNil(t, client)
	assert.False(t, client.RecognizeVehicle)
	assert.False(t, client.ReturnImage)
}
