package openalpr

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"gitlab.com/keithwoelke/parking-police/pkg/photo"
)

const recognizeByBytesURL = "/v2/recognize_bytes"
const country = "us"

type openALPRRecognizeByBytes200Response struct {
	ProcessingTime      *openALPRProcessingTime    `json:"processing_time,omitempty"`
	DataType            string                     `json:"data_type,omitempty"`
	ImageBytesPrefix    string                     `json:"image_bytes_prefix,omitempty"`
	UUID                string                     `json:"uuid,omitempty"`
	RegionsOfInterest   []openALPRRegionOfInterest `json:"regions_of_interest,omitempty"`
	ImageBytes          []byte                     `json:"image_bytes,omitempty"`
	Results             []openALPRPlateDetails     `json:"results,omitempty"`
	ImgWidth            int32                      `json:"img_width,omitempty"`
	ImgHeight           int32                      `json:"img_height,omitempty"`
	CreditCost          int32                      `json:"credit_cost,omitempty"`
	CreditsMonthlyUsed  int32                      `json:"credits_monthly_used,omitempty"`
	CreditsMonthlyTotal int32                      `json:"credits_monthly_total,omitempty"`
	Version             int32                      `json:"version,omitempty"`
	EpochTime           float32                    `json:"epoch_time,omitempty"`
	Error               bool                       `json:"error,omitempty"`
}

type openALPRPlateDetails struct {
	VehicleRegion    *openALPRRegionOfInterest `json:"vehicle_region,omitempty"`
	Vehicle          *openALPRVehicleDetails   `json:"vehicle,omitempty"`
	Plate            string                    `json:"plate,omitempty"`
	Region           string                    `json:"region,omitempty"`
	Coordinates      []openALPRCoordinate      `json:"coordinates,omitempty"`
	Candidates       []openALPRPlateCandidate  `json:"candidates,omitempty"`
	RegionConfidence float32                   `json:"region_confidence,omitempty"`
	MatchesTemplate  int32                     `json:"matches_template,omitempty"`
	RequestedTopn    int32                     `json:"requested_topn,omitempty"`
	ProcessingTimeMs float32                   `json:"processing_time_ms,omitempty"`
	Confidence       float32                   `json:"confidence,omitempty"`
}

type openALPRCoordinate struct {
	X int32 `json:"x,omitempty"`
	Y int32 `json:"y,omitempty"`
}

type openALPRPlateCandidate struct {
	Plate           string  `json:"plate,omitempty"`
	Confidence      float32 `json:"confidence,omitempty"`
	MatchesTemplate int32   `json:"matches_template,omitempty"`
}

type openALPRRegionOfInterest struct {
	X      int32 `json:"x,omitempty"`
	Y      int32 `json:"y,omitempty"`
	Width  int32 `json:"width,omitempty"`
	Height int32 `json:"height,omitempty"`
}

type openALPRVehicleDetails struct {
	Color     []openALPRVehicleCandidate `json:"color,omitempty"`
	Make      []openALPRVehicleCandidate `json:"make,omitempty"`
	MakeModel []openALPRVehicleCandidate `json:"make_model,omitempty"`
	BodyType  []openALPRVehicleCandidate `json:"body_type,omitempty"`
	Year      []openALPRVehicleCandidate `json:"year,omitempty"`
}

type openALPRVehicleCandidate struct {
	Name       string  `json:"name,omitempty"`
	Confidence float32 `json:"confidence,omitempty"`
}

type openALPRProcessingTime struct {
	Total    float32 `json:"total,omitempty"`
	Plates   float32 `json:"plates,omitempty"`
	Vehicles float32 `json:"vehicles,omitempty"`
}

type VehicleAnalysis struct {
	Vehicle *VehicleCandidate `json:"vehicle,omitempty"`
	Image   []byte            `json:"vehicleImage,omitempty"`
	Plate   PlateCandidate    `json:"plate"`
}

type VehicleCandidate struct {
	Make      []VehicleTraitCandidate `json:"make,omitempty"`
	MakeModel []VehicleTraitCandidate `json:"makeModel,omitempty"`
	Year      []VehicleTraitCandidate `json:"year,omitempty"`
	Color     []VehicleTraitCandidate `json:"color,omitempty"`
	BodyType  []VehicleTraitCandidate `json:"bodyType,omitempty"`
}

type VehicleTraitCandidate struct {
	Name       string  `json:"name,omitempty"`
	Confidence float32 `json:"confidence,omitempty"`
}

type PlateNumberCandidate struct {
	Plate      string  `json:"plate,omitempty"`
	Confidence float32 `json:"confidence,omitempty"`
}

type PlateCandidate struct {
	Region     string                 `json:"region,omitempty"`
	Candidates []PlateNumberCandidate `json:"plateCandidates,omitempty"`
	Image      []byte                 `json:"image,omitempty"`
}

func (response *openALPRRecognizeByBytes200Response) toVehicleAnalyses(imageBytes []byte) (vehicleAnalyses []VehicleAnalysis, usage Usage, err error) {
	usage = extractUsage(response)
	vehicleAnalyses, err = convertToVehicleAnalyses(response.Results, imageBytes)

	return vehicleAnalyses, usage, err
}

func extractUsage(recognizeByBytesResponse *openALPRRecognizeByBytes200Response) (usage Usage) {
	usage.CreditCost = int(recognizeByBytesResponse.CreditCost)
	usage.CreditsMonthlyTotal = int(recognizeByBytesResponse.CreditsMonthlyTotal)
	usage.CreditsMonthlyUsed = int(recognizeByBytesResponse.CreditsMonthlyUsed)
	usage.CreditsMonthlyRemaining = int(recognizeByBytesResponse.CreditsMonthlyTotal - recognizeByBytesResponse.CreditsMonthlyUsed)

	return usage
}

func (c *Client) recognizeByBytesRequest(imageBytes []byte) (request *http.Request, err error) {
	base64EncodedImage := base64.StdEncoding.EncodeToString(imageBytes)

	if request, err = http.NewRequest(http.MethodPost, fmt.Sprintf("%s%s", c.BaseURL, recognizeByBytesURL), strings.NewReader(base64EncodedImage)); err != nil {
		return request, errors.WithStack(err)
	}

	recognizeVehicle := boolToInt(c.RecognizeVehicle)
	returnImage := boolToInt(c.ReturnImage)

	query := request.URL.Query()
	query.Add("secret_key", c.SecretKey)
	query.Add("country", country)
	query.Add("recognize_vehicle", strconv.Itoa(recognizeVehicle))
	query.Add("return_image", strconv.Itoa(returnImage))

	request.URL.RawQuery = query.Encode()

	return request, nil
}

func boolToInt(boolValue bool) (intValue int) {
	if boolValue {
		intValue = 1
	}

	return intValue
}

type Usage struct {
	CreditsMonthlyUsed      int
	CreditsMonthlyTotal     int
	CreditCost              int
	CreditsMonthlyRemaining int
}

func convertToVehicleAnalyses(openALPRPlateDetails []openALPRPlateDetails, imageBytes []byte) (vehicleAnalyses []VehicleAnalysis, err error) {
	vehicleAnalyses = make([]VehicleAnalysis, 0)

	for _, openALPRPlateDetail := range openALPRPlateDetails {
		var vehicleAnalysis VehicleAnalysis
		if vehicleAnalysis, err = convertToVehicleAnalysis(openALPRPlateDetail, imageBytes); err != nil {
			return vehicleAnalyses, err
		}

		vehicleAnalyses = append(vehicleAnalyses, vehicleAnalysis)
	}

	return vehicleAnalyses, nil
}

// AnalyzeImage returns a list of VehicleAnalysis results, one for every vehicle identified in the provided image. The
// second return is metadata containing the request cost, credits used, credits remaining, and total credits
// allotted. All balances reflect usage within the current calendar month. The third return contains image metadata. The
// found, and final, return is an error or nil if the function executed successfully.
func (c *Client) AnalyzeImage(imageBytes []byte) (vehicleAnalyses []VehicleAnalysis, usage Usage, imageMetadata photo.Metadata, err error) {
	httpRequest, err := c.recognizeByBytesRequest(imageBytes)
	if err != nil {
		return vehicleAnalyses, usage, imageMetadata, err
	}

	httpResponse, err := c.requestWithRetry(httpRequest)
	if err != nil {
		return vehicleAnalyses, usage, imageMetadata, err
	}

	responseBytes, err := ioutil.ReadAll(httpResponse.Body)
	if err != nil {
		return vehicleAnalyses, usage, imageMetadata, errors.WithStack(err)
	}
	defer func() {
		if closeErr := httpResponse.Body.Close(); closeErr != nil {
			log.Warnf(fmt.Sprintf("%+v", errors.WithStack(closeErr)))
		}
	}()

	recognizeByBytesResponse := new(openALPRRecognizeByBytes200Response)
	if err = json.Unmarshal(responseBytes, recognizeByBytesResponse); err != nil {
		return vehicleAnalyses, usage, imageMetadata, errors.WithStack(err)
	}

	curTime := time.Now()
	imageMetadata, err = photo.ImageMetadata(imageBytes)
	if err != nil {
		imageMetadata = photo.Metadata{
			DateTime: &curTime,
		}
	}

	analyses, usage, err := recognizeByBytesResponse.toVehicleAnalyses(imageBytes)
	return analyses, usage, imageMetadata, err
}

func convertToVehicleAnalysis(plateDetail openALPRPlateDetails, imageBytes []byte) (vehicleAnalysis VehicleAnalysis, err error) {
	var vehicleImageBytes, plateImageBytes []byte
	if vehicleImageBytes, plateImageBytes, err = cropImages(imageBytes, plateDetail); err != nil {
		return vehicleAnalysis, err
	}

	vehicleAnalysis = VehicleAnalysis{
		Vehicle: convertToVehicleCandidate(plateDetail.Vehicle),
		Image:   vehicleImageBytes,
		Plate:   convertToPlateCandidate(plateDetail, plateImageBytes),
	}

	return vehicleAnalysis, err
}

func convertToPlateCandidate(openALPRPlateDetails openALPRPlateDetails, plateImageBytes []byte) (plateCandidate PlateCandidate) {
	return PlateCandidate{
		Region:     openALPRPlateDetails.Region,
		Candidates: convertToPlateNumberCandidates(openALPRPlateDetails.Candidates),
		Image:      plateImageBytes,
	}
}

func cropImages(imageBytes []byte, openALPRPlateDetails openALPRPlateDetails) (vehicleImageBytes, plateImageBytes []byte, err error) {
	if vehicleImageBytes, err = cropVehicle(imageBytes, openALPRPlateDetails.VehicleRegion); err != nil {
		return vehicleImageBytes, plateImageBytes, err
	}

	if plateImageBytes, err = cropPlate(imageBytes, openALPRPlateDetails.Coordinates); err != nil {
		return vehicleImageBytes, plateImageBytes, err
	}

	return vehicleImageBytes, plateImageBytes, nil
}

func cropVehicle(imageBytes []byte, openALPRRegionOfInterest *openALPRRegionOfInterest) (vehicleImage []byte, err error) {
	x, y, width, height := calculateVehicleLocation(openALPRRegionOfInterest)

	return photo.Crop(imageBytes, x, y, width, height)
}

func cropPlate(imageBytes []byte, openALPRCoordinates []openALPRCoordinate) (plateImageBytes []byte, err error) {
	x, y, width, height := calculatePlateLocation(openALPRCoordinates)

	return photo.Crop(imageBytes, x, y, width, height)
}

func calculatePlateLocation(openALPRCoordinates []openALPRCoordinate) (x, y, width, height int) {
	upperLeft := openALPRCoordinates[0]
	upperRight := openALPRCoordinates[1]
	lowerRight := openALPRCoordinates[2]
	lowerLeft := openALPRCoordinates[3]

	x1 := float64(upperLeft.X)
	y1 := float64(upperLeft.Y)
	x2 := float64(upperRight.X)
	y2 := float64(upperRight.Y)
	x3 := float64(lowerRight.X)
	y3 := float64(lowerRight.Y)
	x4 := float64(lowerLeft.X)
	y4 := float64(lowerLeft.Y)

	x = int(math.Min(math.Min(math.Min(x1, x2), x3), x4))
	y = int(math.Min(math.Min(math.Min(y1, y2), y3), y4))
	largestX := int(math.Max(math.Max(math.Max(x1, x2), x3), x4))
	largestY := int(math.Max(math.Max(math.Max(y1, y2), y3), y4))

	width = largestX - x
	height = largestY - y

	return x, y, width, height
}

func calculateVehicleLocation(openALPRRegionOfInterest *openALPRRegionOfInterest) (x, y, width, height int) {
	if openALPRRegionOfInterest != nil {
		x = int(openALPRRegionOfInterest.X)
		y = int(openALPRRegionOfInterest.Y)

		width = int(openALPRRegionOfInterest.Width)
		height = int(openALPRRegionOfInterest.Height)
	}

	return x, y, width, height
}

func convertToPlateNumberCandidates(openALPRPlateCandidates []openALPRPlateCandidate) (plateNumberCandidates []PlateNumberCandidate) {
	for _, openALPRPlateCandidate := range openALPRPlateCandidates {
		plateNumberCandidate := convertToPlateNumberCandidate(openALPRPlateCandidate)
		plateNumberCandidates = append(plateNumberCandidates, plateNumberCandidate)
	}

	return plateNumberCandidates
}

func convertToVehicleCandidate(openALPRVehicleDetails *openALPRVehicleDetails) (vehicleCandidate *VehicleCandidate) {
	if openALPRVehicleDetails != nil {
		vehicleCandidate = &VehicleCandidate{
			Make:      convertToVehicleTraitCandidates(openALPRVehicleDetails.Make),
			MakeModel: convertToVehicleTraitCandidates(openALPRVehicleDetails.MakeModel),
			Year:      convertToVehicleTraitCandidates(openALPRVehicleDetails.Year),
			Color:     convertToVehicleTraitCandidates(openALPRVehicleDetails.Color),
			BodyType:  convertToVehicleTraitCandidates(openALPRVehicleDetails.BodyType),
		}
	}

	return vehicleCandidate
}

func convertToPlateNumberCandidate(openALPRPlateCandidate openALPRPlateCandidate) (plateNumberCandidate PlateNumberCandidate) {
	plateNumberCandidate.Plate = openALPRPlateCandidate.Plate
	plateNumberCandidate.Confidence = openALPRPlateCandidate.Confidence

	return plateNumberCandidate
}

func convertToVehicleTraitCandidates(openALPRVehicleCandidates []openALPRVehicleCandidate) (vehicleTraitCandidates []VehicleTraitCandidate) {
	for _, openALPRVehicleCandidate := range openALPRVehicleCandidates {
		vehicleTraitCandidate := convertToVehicleTraitCandidate(openALPRVehicleCandidate)
		vehicleTraitCandidates = append(vehicleTraitCandidates, vehicleTraitCandidate)
	}

	return vehicleTraitCandidates
}

func convertToVehicleTraitCandidate(openALPRVehicleCandidate openALPRVehicleCandidate) (vehicleTraitCandidate VehicleTraitCandidate) {
	vehicleTraitCandidate.Confidence = openALPRVehicleCandidate.Confidence
	vehicleTraitCandidate.Name = openALPRVehicleCandidate.Name

	return vehicleTraitCandidate
}
