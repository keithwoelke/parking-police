// Package openalpr provides a client for interacting with the OpenALPR API. It will retry on 5XX status codes
// automatically.
package openalpr

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"github.com/cenkalti/backoff"
	"github.com/pkg/errors"
)

const openALPRBaseURL = "https://api.openalpr.com"

const maxRetryDuration = 10 * time.Second
const httpRequestTimeout = 5 * time.Second

// A Client is an OpenALPR client.
type Client struct {
	retryStrategy    backoff.BackOff
	httpClient       *http.Client
	BaseURL          string
	SecretKey        string
	RecognizeVehicle bool
	ReturnImage      bool
}

// New creates a new OpenALPR client.
func New(secretKey string) (client *Client, err error) {
	c := &Client{
		RecognizeVehicle: false,
		ReturnImage:      false,
		httpClient:       httpClient(),
		retryStrategy:    retryStrategy(),
		BaseURL:          openALPRBaseURL,
		SecretKey:        secretKey,
	}

	return c, nil
}

func httpClient() *http.Client {
	return &http.Client{
		Timeout: httpRequestTimeout,
	}
}

func retryStrategy() (retryStrategy backoff.BackOff) {
	exponentialBackOffRetryStrategy := backoff.NewExponentialBackOff()
	exponentialBackOffRetryStrategy.MaxElapsedTime = maxRetryDuration

	return exponentialBackOffRetryStrategy
}

type errorResponse struct {
	Error     string `json:"error"`
	ErrorCode int    `json:"error_code"`
}

func (c *Client) requestWithRetry(request *http.Request) (response *http.Response, err error) {
	retryable := func() (err error) {
		if response, err = c.httpClient.Do(request); err != nil {
			return backoff.Permanent(errors.WithStack(err))
		}

		return mapStatusCodeToError(response.StatusCode)
	}

	err = backoff.Retry(retryable, c.retryStrategy)
	if err != nil {
		bodyBytes, readErr := ioutil.ReadAll(response.Body)
		if readErr != nil {
			return response, errors.WithStack(readErr)
		}

		var errResponse errorResponse
		if unmarshalErr := json.Unmarshal(bodyBytes, &errResponse); unmarshalErr != nil {
			return response, unmarshalErr
		}

		return response, errors.Wrap(err, strings.ToLower(errResponse.Error))
	}

	return response, err
}

func mapStatusCodeToError(statusCode int) (err error) {
	if statusCode >= 500 && statusCode <= 599 {
		return errors.Errorf("request failed with retryable status code %d", statusCode)
	} else if statusCode >= 400 && statusCode <= 499 {
		return backoff.Permanent(errors.Errorf("request failed with non-retryable status code %d", statusCode))
	}

	return nil
}
