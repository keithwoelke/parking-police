package openalpr_test

import (
	"encoding/base64"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/keithwoelke/parking-police/pkg/openalpr"
)

type errorResponse struct {
	Error     string `json:"error"`
	ErrorCode int    `json:"error_code"`
}

func TestClient_AnalyzeImage(t *testing.T) {
	var responses = []struct {
		description          string
		responsePath         string
		expectedAnalysisPath string
		numExpectedResults   int
	}{
		{
			description:          "multiple results",
			responsePath:         "../../test/testdata/open-alpr/responses/multiple-results.json",
			expectedAnalysisPath: "testdata/analyses/multiple-results.golden",
			numExpectedResults:   2,
		},
		{
			description:          "zero results",
			responsePath:         "../../test/testdata/open-alpr/responses/zero-results.json",
			expectedAnalysisPath: "testdata/analyses/zero-results.golden",
			numExpectedResults:   0,
		},
		{
			description:          "no vehicle information",
			responsePath:         "../../test/testdata/open-alpr/responses/no-vehicle-information.json",
			expectedAnalysisPath: "testdata/analyses/no-vehicle-information.golden",
			numExpectedResults:   1,
		},
		{
			description:          "vehicle information",
			responsePath:         "../../test/testdata/open-alpr/responses/vehicle-information.json",
			expectedAnalysisPath: "testdata/analyses/vehicle-information.golden",
			numExpectedResults:   1,
		},
	}

	const secretKey = "12345"
	for _, tt := range responses {
		tt := tt
		t.Run(tt.description, func(t *testing.T) {
			t.Parallel()

			imageBytes, err := ioutil.ReadFile("../../test/testdata/images/vehicle.png")
			require.NoError(t, err)

			testServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				requestBytes, requestReadErr := ioutil.ReadAll(r.Body)
				require.NoError(t, requestReadErr)

				assert.Equal(t, base64.StdEncoding.EncodeToString(imageBytes), string(requestBytes), "image bytes should be base64 encoded")

				responseBytes, responseReadErr := ioutil.ReadFile(tt.responsePath)
				require.NoError(t, responseReadErr)
				assert.Equal(t, secretKey, r.URL.Query().Get("secret_key"))

				w.WriteHeader(http.StatusOK)
				_, err = w.Write(responseBytes)
				require.NoError(t, err)
			}))
			defer func() { testServer.Close() }()

			client, err := openalpr.New(secretKey)
			require.NoError(t, err)
			client.BaseURL = testServer.URL

			vehicleAnalyses, _, _, err := client.AnalyzeImage(imageBytes)
			require.NoError(t, err)
			assert.Equal(t, tt.numExpectedResults, len(vehicleAnalyses))

			actualBytes, err := json.Marshal(vehicleAnalyses)
			require.NoError(t, err)

			expectedBytes, err := ioutil.ReadFile(tt.expectedAnalysisPath)
			require.NoError(t, err)
			assert.Equal(t, string(expectedBytes), string(actualBytes))
		})
	}
}

func TestClient_AnalyzeImageWithInvalidImageShouldReturnError(t *testing.T) {
	const openALPRErrorMessage = "oalpr error"

	testServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		const statusCode = http.StatusBadRequest

		bodyBytes, err := json.Marshal(errorResponse{Error: openALPRErrorMessage, ErrorCode: statusCode})
		require.NoError(t, err)

		w.WriteHeader(statusCode)

		_, err = w.Write(bodyBytes)
		require.NoError(t, err)
	}))
	defer func() { testServer.Close() }()

	client, err := openalpr.New("")
	require.NoError(t, err)
	client.BaseURL = testServer.URL

	_, _, _, err = client.AnalyzeImage([]byte{})
	require.Error(t, err)

	assert.Contains(t, err.Error(), openALPRErrorMessage)
}
