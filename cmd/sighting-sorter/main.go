package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"gitlab.com/keithwoelke/parking-police/internal/db"
	"gitlab.com/keithwoelke/parking-police/internal/server"
	"gitlab.com/keithwoelke/parking-police/internal/server/middleware"
	"gitlab.com/keithwoelke/parking-police/pkg/openalpr"
)

type appConfig struct {
	deploymentDate    string
	buildNumber       string
	openALPRSecretKey string
	port              string
	ginMode           string
	apiKey            string
}

func main() {
	config := readAppConfig()

	log.SetFormatter(&log.JSONFormatter{})
	gin.SetMode(config.ginMode)

	openALPRClient, err := openalpr.New(config.openALPRSecretKey)
	if err != nil {
		log.Fatalf("%+v", err)
	}

	dbClient, err := db.New(context.Background())
	if err != nil {
		log.Fatalf("%+v", err)
	}

	router, err := server.NewRouter(openALPRClient, dbClient, middleware.DeploymentMetadata{DeploymentDate: config.deploymentDate, BuildNumber: config.buildNumber}, config.apiKey)
	if err != nil {
		log.Fatalf("%+v", err)
	}

	srv := &http.Server{
		Addr:    config.port,
		Handler: router,
	}

	go func() {
		log.Infoln("Starting Parking Police")
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			err = errors.WithMessage(err, "server shut down unexpectedly")
			log.Fatalf("%+v", err)
		}
	}()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt, syscall.SIGTERM)
	<-quit

	log.Infoln("Shutting down server...")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err := srv.Shutdown(ctx); err != nil {
		err = errors.WithMessage(err, "error occurred while shutting down server")
		log.Errorf("%+v", err)
		log.Exit(1)
	}

	<-ctx.Done()

	log.Infoln("Server exiting.")
}

func readAppConfig() (config appConfig) {
	deploymentDate, exists := os.LookupEnv("DEPLOYMENT_DATE")
	if !exists {
		err := errors.New("DEPLOYMENT_DATE is not set")
		log.Fatalf("%+v", err)
	}

	buildNumber, exists := os.LookupEnv("BUILD_NUMBER")
	if !exists {
		err := errors.New("BUILD_NUMBER is not set")
		log.Fatalf("%+v", err)
	}

	openALPRSecretKey, exists := os.LookupEnv("OALPR_SECRET_KEY")
	if !exists {
		err := errors.New("OALPR_SECRET_KEY is not set")
		log.Fatalf("%+v", err)
	}

	port, exists := os.LookupEnv("PORT")
	if !exists {
		port = "8080"
	}

	ginMode, exists := os.LookupEnv("GIN_MODE")
	if !exists {
		ginMode = gin.TestMode
	}

	apiKey, exists := os.LookupEnv("API_KEY")
	if !exists {
		err := errors.New("API_KEY is not set")
		log.Fatalf("%+v", err)
	}

	return appConfig{
		deploymentDate:    deploymentDate,
		buildNumber:       buildNumber,
		openALPRSecretKey: openALPRSecretKey,
		port:              fmt.Sprintf(":%s", port),
		ginMode:           ginMode,
		apiKey:            apiKey,
	}
}
