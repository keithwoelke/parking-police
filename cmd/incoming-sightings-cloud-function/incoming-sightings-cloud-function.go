package incomingsightings //nolint:typecheck

import (
	"context"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	log "github.com/sirupsen/logrus"

	"cloud.google.com/go/storage"
	_ "github.com/GoogleCloudPlatform/functions-framework-go/funcframework" // Required for Cloud Functions
)

type GCSEvent struct {
	TimeCreated             time.Time              `json:"timeCreated"`
	TimeStorageClassUpdated time.Time              `json:"timeStorageClassUpdated"`
	RetentionExpirationTime time.Time              `json:"retentionExpirationTime"`
	Updated                 time.Time              `json:"updated"`
	Metadata                map[string]interface{} `json:"metadata"`
	CustomerEncryption      struct {
		EncryptionAlgorithm string `json:"encryptionAlgorithm"`
		KeySha256           string `json:"keySha256"`
	}
	Kind               string `json:"kind"`
	ContentType        string `json:"contentType"`
	Metageneration     string `json:"metageneration"`
	Generation         string `json:"generation"`
	KMSKeyName         string `json:"kmsKeyName"`
	Bucket             string `json:"bucket"`
	Name               string `json:"name"`
	StorageClass       string `json:"storageClass"`
	SelfLink           string `json:"selfLink"`
	Size               string `json:"size"`
	MD5Hash            string `json:"md5Hash"`
	MediaLink          string `json:"mediaLink"`
	ContentEncoding    string `json:"contentEncoding"`
	ContentDisposition string `json:"contentDisposition"`
	CacheControl       string `json:"cacheControl"`
	ID                 string `json:"id"`
	CRC32C             string `json:"crc32c"`
	Etag               string `json:"etag"`
	ResourceState      string `json:"resourceState"`
	ComponentCount     int    `json:"componentCount"`
	EventBasedHold     bool   `json:"eventBasedHold"`
	TemporaryHold      bool   `json:"temporaryHold"`
}

func AddSighting(ctx context.Context, pubSubMessage GCSEvent) error {
	return addSighting(pubSubMessage.Bucket, pubSubMessage.Name)
}

func addSighting(bucket, object string) error {
	ctx := context.Background()
	client, err := storage.NewClient(ctx)
	if err != nil {
		log.Errorf("storage.NewClient: %v", err)
		return fmt.Errorf("storage.NewClient: %+v", err)
	}
	defer client.Close()

	rc, err := client.Bucket(bucket).Object(object).NewReader(ctx)
	if err != nil {
		log.Errorf("Object(%q).NewReader: %v", object, err)
		return fmt.Errorf("Object(%q).NewReader: %+v", object, err)
	}
	defer rc.Close()

	req, err := http.NewRequest(http.MethodPost, "http://parking-police.appspot.com/sighting", rc)
	if err != nil {
		log.Errorf("http.NewRequest(http.MethodPost, http://parking-police.appspot.com/sighting, nil): %+v", err)
		return fmt.Errorf("http.NewRequest(http.MethodPost, http://parking-police.appspot.com/sighting, nil): %+v", err)
	}

	req.SetBasicAuth("keith", "01189998819991197253")

	httpClient := new(http.Client)
	resp, err := httpClient.Do(req)
	if err != nil {
		log.Errorf("http.Post(http://parking-police.appspot.com/sighting, application/octet-stream, %+v): %+v", rc, err)
		return fmt.Errorf("http.Post(http://parking-police.appspot.com/sighting, application/octet-stream, %v): %+v", rc, err)
	}

	body, err := ioutil.ReadAll(resp.Body)
	log.Infof("Response: %d, %+v, %+v", resp.StatusCode, string(body), err)

	return nil
}
