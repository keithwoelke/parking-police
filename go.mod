module gitlab.com/keithwoelke/parking-police

go 1.16

require (
	cloud.google.com/go/datastore v1.6.0
	cloud.google.com/go/storage v1.10.0
	github.com/GoogleCloudPlatform/functions-framework-go v1.5.3
	github.com/cenkalti/backoff v2.2.1+incompatible
	github.com/dsoprea/go-exif v0.0.0-20210625224831-a6301f85c82b
	github.com/gin-gonic/gin v1.7.7
	github.com/googleapis/google-cloud-go-testing v0.0.0-20210719221736-1c9a4c676720
	github.com/oliamb/cutter v0.2.2
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.7.1
	golang.org/x/image v0.0.0-20220413100746-70e8d0d3baa9
)
