package analysis_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/keithwoelke/parking-police/internal/analysis"
	"gitlab.com/keithwoelke/parking-police/pkg/openalpr"
)

func TestFinalize(t *testing.T) {
	vehicleImage := "vehicle image"
	plateImage := "plate image"

	vehicleAnalysis := openalpr.VehicleAnalysis{
		Image: []byte(vehicleImage),
		Plate: openalpr.PlateCandidate{
			Region: "region",
			Candidates: []openalpr.PlateNumberCandidate{
				{
					Plate: "plate-1",
				},
				{
					Plate: "plate-2",
				},
			},
			Image: []byte(plateImage),
		},
	}

	vehicle := analysis.Finalize(vehicleAnalysis)
	vehiclePlate := vehicle.Plate

	assert.Equal(t, "region", vehiclePlate.Region)
	assert.Equal(t, "plate-1", vehiclePlate.Number)
	assert.Equal(t, []byte(vehicleImage), vehicle.Image)
	assert.Equal(t, []byte(plateImage), vehiclePlate.Image)
}
