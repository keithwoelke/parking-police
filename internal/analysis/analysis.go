package analysis

import "gitlab.com/keithwoelke/parking-police/pkg/openalpr"

type Vehicle struct {
	Plate Plate
	Image []byte
}

type Plate struct {
	Number string
	Region string
	Image  []byte
}

func Finalize(analysis openalpr.VehicleAnalysis) (vehicle Vehicle) {
	return Vehicle{
		Plate: Plate{
			Region: analysis.Plate.Region,
			Number: analysis.Plate.Candidates[0].Plate,
			Image:  analysis.Plate.Image,
		},
		Image: analysis.Image,
	}
}
