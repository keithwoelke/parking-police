package server

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/keithwoelke/parking-police/internal/db"
	"gitlab.com/keithwoelke/parking-police/internal/server/handlers"
	"gitlab.com/keithwoelke/parking-police/internal/server/middleware"
	"gitlab.com/keithwoelke/parking-police/pkg/openalpr"
)

func NewRouter(openALPRClient *openalpr.Client, dbClient *db.Client, deploymentMetadata middleware.DeploymentMetadata, apiKey string) (router *gin.Engine, err error) {
	router = gin.New()
	router.Use(gin.Logger(), middleware.Metadata(deploymentMetadata), middleware.Errors, gin.Recovery())

	router.GET("/_ah/health", handlers.Ping())
	router.GET("/ping", handlers.Ping())
	router.POST("/sighting", gin.BasicAuth(gin.Accounts{"keith": apiKey}), handlers.AddSighting(openALPRClient, dbClient))

	return router, nil
}
