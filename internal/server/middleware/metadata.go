package middleware

import (
	"github.com/gin-gonic/gin"
)

func Metadata(deploymentMetadata DeploymentMetadata) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		ctx.Header("deployment-date", deploymentMetadata.DeploymentDate)
		ctx.Header("build-number", deploymentMetadata.BuildNumber)
	}
}

type DeploymentMetadata struct {
	BuildNumber    string
	DeploymentDate string
}
