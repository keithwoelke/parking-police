package middleware_test

import (
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"gitlab.com/keithwoelke/parking-police/internal/server/middleware"
)

func TestMetadata(t *testing.T) {
	gin.SetMode(gin.TestMode)

	deploymentDate := time.Now().String()
	buildNumber := "12345"

	recorder := httptest.NewRecorder()
	_, router := gin.CreateTestContext(recorder)
	req := httptest.NewRequest(http.MethodGet, "/", nil)

	router.Use(middleware.Metadata(middleware.DeploymentMetadata{DeploymentDate: deploymentDate, BuildNumber: buildNumber}))
	router.ServeHTTP(recorder, req)

	assert.Equal(t, deploymentDate, recorder.Header().Get("deployment-date"))
	assert.Equal(t, buildNumber, recorder.Header().Get("build-number"))
}
