package middleware

import (
	"net/http"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
)

type coder interface {
	Code() int
}

func Errors(ctx *gin.Context) {
	ctx.Next()

	err := ctx.Errors.Last()
	if err == nil {
		return
	}

	code := http.StatusInternalServerError

	if metadata, ok := err.Meta.(coder); ok {
		code = metadata.Code()
	} else {
		log.Errorf("%+v", err.Err)
	}

	if err.Type == gin.ErrorTypePublic {
		ctx.String(code, err.Error())
	} else {
		ctx.String(code, "something went wrong")
	}
}
