package middleware_test

import (
	"errors"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/keithwoelke/parking-police/internal/server/middleware"
)

type appError struct {
	error
	code int
}

func (ae *appError) Code() (code int) {
	return ae.code
}

func TestErrorsShouldSet500InternalServerWhenHandlerSetsPrivateError(t *testing.T) {
	gin.SetMode(gin.TestMode)

	const publicErrorReason = "error reason"

	var errorConfigs = []struct {
		metadata           interface{}
		description        string
		expectedResponse   string
		expectedStatusCode int
		errorType          gin.ErrorType
	}{
		{
			description:        "private with metadata",
			metadata:           &appError{code: http.StatusTeapot},
			expectedStatusCode: http.StatusTeapot,
			errorType:          gin.ErrorTypePrivate,
			expectedResponse:   "something went wrong",
		},
		{
			description:        "private without metadata",
			expectedStatusCode: http.StatusInternalServerError,
			errorType:          gin.ErrorTypePrivate,
			expectedResponse:   "something went wrong",
		},
		{
			description:        "public with metadata",
			metadata:           &appError{code: http.StatusTeapot},
			expectedStatusCode: http.StatusTeapot,
			errorType:          gin.ErrorTypePublic,
			expectedResponse:   publicErrorReason,
		},
		{
			description:        "public without metadata",
			expectedStatusCode: http.StatusInternalServerError,
			errorType:          gin.ErrorTypePublic,
			expectedResponse:   publicErrorReason,
		},
	}

	for _, tt := range errorConfigs {
		tt := tt
		t.Run(tt.description, func(t *testing.T) {
			t.Parallel()

			recorder := httptest.NewRecorder()
			_, router := gin.CreateTestContext(recorder)
			req := httptest.NewRequest(http.MethodGet, "/", nil)

			router.Use(middleware.Errors)
			router.GET("/", func(context *gin.Context) {
				_ = context.Error(errors.New(publicErrorReason)).SetMeta(tt.metadata).SetType(tt.errorType)
			})

			router.ServeHTTP(recorder, req)

			bodyBytes, err := ioutil.ReadAll(recorder.Body)
			require.NoError(t, err)

			assert.Equal(t, tt.expectedResponse, string(bodyBytes))
			assert.Equal(t, tt.expectedStatusCode, recorder.Code)
		})
	}
}

func TestErrorsShouldSet200OKWhenHandlerDoesNotSetError(t *testing.T) {
	gin.SetMode(gin.TestMode)

	recorder := httptest.NewRecorder()
	_, router := gin.CreateTestContext(recorder)
	req := httptest.NewRequest(http.MethodGet, "/", nil)

	router.Use(middleware.Errors)
	router.GET("/", func(context *gin.Context) {})

	router.ServeHTTP(recorder, req)

	assert.Equal(t, http.StatusOK, recorder.Code)
}
