package handlers_test

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"sort"
	"strconv"
	"strings"
	"testing"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/keithwoelke/parking-police/internal/db"
	"gitlab.com/keithwoelke/parking-police/internal/db/fake"
	"gitlab.com/keithwoelke/parking-police/internal/server/handlers"
	"gitlab.com/keithwoelke/parking-police/pkg/openalpr"
)

type sighting struct {
	PlateRegion string `json:"region,omitempty"`
	PlateNumber string `json:"number,omitempty"`
	Image       []byte `json:"image,omitempty"`
}

type sightingDate struct {
	Date time.Time `json:"date,omitempty"`
}

type coder interface {
	Code() int
}

func TestAddSighting(t *testing.T) {
	gin.SetMode(gin.TestMode)

	responses := []struct {
		description             string
		openALPRResponse        string
		expectedDatabaseEntries string
		numExpectedSightings    int
		creditCost              int
	}{
		{
			description:             "zero results",
			openALPRResponse:        "../../../test/testdata/open-alpr/responses/zero-results.json",
			expectedDatabaseEntries: "testdata/sightings/zero-results.golden",
			numExpectedSightings:    0,
			creditCost:              1,
		},
		{
			description:             "multiple results",
			openALPRResponse:        "../../../test/testdata/open-alpr/responses/multiple-results.json",
			expectedDatabaseEntries: "testdata/sightings/multiple-results.golden",
			numExpectedSightings:    2,
			creditCost:              1,
		},
		{
			description:             "vehicle information",
			openALPRResponse:        "../../../test/testdata/open-alpr/responses/vehicle-information.json",
			expectedDatabaseEntries: "testdata/sightings/vehicle-information.golden",
			numExpectedSightings:    1,
			creditCost:              2,
		},
		{
			description:             "no vehicle information",
			openALPRResponse:        "../../../test/testdata/open-alpr/responses/no-vehicle-information.json",
			expectedDatabaseEntries: "testdata/sightings/no-vehicle-information.golden",
			numExpectedSightings:    1,
			creditCost:              1,
		},
	}

	for _, tt := range responses {
		tt := tt
		t.Run(tt.description, func(t *testing.T) {
			t.Parallel()

			recorder := httptest.NewRecorder()
			_, router := gin.CreateTestContext(recorder)

			testServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				response, err := os.Open(tt.openALPRResponse)
				require.NoError(t, err)

				responseBytes, err := ioutil.ReadAll(response)
				require.NoError(t, err)

				w.WriteHeader(http.StatusOK)
				_, err = w.Write(responseBytes)
				require.NoError(t, err)
			}))
			defer func() { testServer.Close() }()

			openALPRClient, err := openalpr.New("")
			require.NoError(t, err)
			openALPRClient.BaseURL = testServer.URL

			dbClient := new(db.Client)
			require.NoError(t, err)
			dbClient.DatastoreClient = fake.New()

			imageBytes, err := ioutil.ReadFile("../../../test/testdata/images/vehicle.png")
			require.NoError(t, err)

			router.GET("/", handlers.AddSighting(openALPRClient, dbClient))
			request, err := http.NewRequest(http.MethodGet, "/", bytes.NewReader(imageBytes))
			require.NoError(t, err)

			router.ServeHTTP(recorder, request)
			assert.Equal(t, http.StatusCreated, recorder.Code)

			assert.Equal(t, strconv.Itoa(tt.creditCost), recorder.Header().Get("credit-cost"))
			assert.Equal(t, strconv.Itoa(100-tt.creditCost), recorder.Header().Get("credits-remaining"))

			databaseSightings := dbClient.DatastoreClient.(*fake.Client).M
			assert.Equal(t, tt.numExpectedSightings, len(databaseSightings))

			var databaseSightingsWithoutDates []sighting
			var databaseSightingsDates []sightingDate
			for _, v := range databaseSightings {
				var databaseSightingBytes []byte
				databaseSightingBytes, err = json.Marshal(v)
				require.NoError(t, err)

				var databaseSighting sighting
				err = json.Unmarshal(databaseSightingBytes, &databaseSighting)
				require.NoError(t, err)

				var databaseSightingDate sightingDate
				err = json.Unmarshal(databaseSightingBytes, &databaseSightingDate)
				require.NoError(t, err)

				databaseSightingsWithoutDates = append(databaseSightingsWithoutDates, databaseSighting)
				databaseSightingsDates = append(databaseSightingsDates, databaseSightingDate)
			}

			sort.Slice(databaseSightingsWithoutDates, func(i, j int) bool {
				return databaseSightingsWithoutDates[i].PlateRegion < databaseSightingsWithoutDates[j].PlateRegion
			})

			actualDatabaseEntriesWithoutDates, err := json.Marshal(databaseSightingsWithoutDates)
			require.NoError(t, err)

			expectedDatabaseEntriesWithoutDates, err := ioutil.ReadFile(tt.expectedDatabaseEntries)
			require.NoError(t, err)

			assert.Equal(t, string(expectedDatabaseEntriesWithoutDates), string(actualDatabaseEntriesWithoutDates))

			for _, databaseSightingDate := range databaseSightingsDates {
				assert.True(t, databaseSightingDate.Date.After(time.Now().Add(-2*time.Second)))
				assert.True(t, databaseSightingDate.Date.Before(time.Now()))
			}
		})
	}
}

func TestAddSightingWithNoImage(t *testing.T) {
	gin.SetMode(gin.TestMode)

	recorder := httptest.NewRecorder()
	_, router := gin.CreateTestContext(recorder)

	router.Use(func(ctx *gin.Context) {
		ctx.Next()

		assert.Equal(t, 1, len(ctx.Errors.Errors()))

		err := ctx.Errors.Last()
		require.Error(t, err)

		metadata, ok := err.Meta.(coder)
		require.True(t, ok)

		assert.Equal(t, http.StatusBadRequest, metadata.Code())
		assert.Equal(t, "image is required", err.Error())
	})
	router.GET("/", handlers.AddSighting(nil, nil))

	request, err := http.NewRequest(http.MethodGet, "/", strings.NewReader(""))
	require.NoError(t, err)

	router.ServeHTTP(recorder, request)
}
