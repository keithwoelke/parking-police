package handlers

import (
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"gitlab.com/keithwoelke/parking-police/internal/analysis"
	"gitlab.com/keithwoelke/parking-police/internal/db"
	"gitlab.com/keithwoelke/parking-police/pkg/openalpr"
)

type appError struct {
	error
	code int
}

func (ae *appError) Code() (code int) {
	return ae.code
}

func AddSighting(openALPRClient *openalpr.Client, dbClient *db.Client) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		imageBytes, err := ioutil.ReadAll(ctx.Request.Body)
		if err != nil {
			_ = ctx.Error(err)
			return
		}

		if len(imageBytes) == 0 {
			_ = ctx.Error(errors.New("image is required")).SetType(gin.ErrorTypePublic).SetMeta(&appError{code: http.StatusBadRequest})
			return
		}

		vehicleAnalyses, usage, imageMetadata, err := openALPRClient.AnalyzeImage(imageBytes)
		if err != nil {
			_ = ctx.Error(err)
		}

		ctx.Header("credit-cost", strconv.Itoa(usage.CreditCost))
		ctx.Header("credits-remaining", strconv.Itoa(usage.CreditsMonthlyRemaining))

		for _, vehicleAnalysis := range vehicleAnalyses {
			key, err := dbClient.WriteSighting(ctx, imageMetadata.DateTime.UTC(), analysis.Finalize(vehicleAnalysis))
			if err != nil {
				_ = ctx.Error(err)
				return
			}

			log.Infof("Wrote sighting %d.", key.ID)
		}

		ctx.Status(http.StatusCreated)
	}
}
