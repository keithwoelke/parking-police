package handlers

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func Ping() (handler gin.HandlerFunc) {
	return func(ctx *gin.Context) {
		ctx.String(http.StatusOK, "ok")
	}
}
