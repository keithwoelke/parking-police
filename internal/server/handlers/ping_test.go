package handlers_test

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/keithwoelke/parking-police/internal/server/handlers"
)

func TestPing(t *testing.T) {
	gin.SetMode(gin.TestMode)

	recorder := httptest.NewRecorder()
	_, router := gin.CreateTestContext(recorder)

	router.GET("/", handlers.Ping())
	request, err := http.NewRequest(http.MethodGet, "/", nil)
	require.NoError(t, err)

	router.ServeHTTP(recorder, request)
	assert.Equal(t, http.StatusOK, recorder.Code)

	bodyBytes, err := ioutil.ReadAll(recorder.Body)
	require.NoError(t, err)

	assert.Equal(t, "ok", string(bodyBytes))
}
