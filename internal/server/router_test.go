package server_test

import (
	"fmt"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/keithwoelke/parking-police/internal/server"
	"gitlab.com/keithwoelke/parking-police/internal/server/middleware"
)

type route struct {
	method string
	path   string
}

func allRoutes() []route {
	return []route{
		{http.MethodGet, "/_ah/health"},
		{http.MethodGet, "/ping"},
		{http.MethodPost, "/sighting"},
	}
}

func TestNewRouterHasExpectedRoutes(t *testing.T) {
	for _, tt := range allRoutes() {
		tt := tt
		t.Run(fmt.Sprintf("%s %s", tt.method, tt.path), func(t *testing.T) {
			t.Parallel()

			router, err := server.NewRouter(nil, nil, middleware.DeploymentMetadata{}, "")
			require.NoError(t, err)
			assert.Equal(t, len(allRoutes()), len(router.Routes()), "number of expected routes differs from actual number of routes")

			var hasMatch bool
			for _, actualRoute := range router.Routes() {
				if actualRoute.Path == tt.path && actualRoute.Method == tt.method {
					hasMatch = true
				}
			}

			assert.True(t, hasMatch, "expected route %s %s should be added to the gin router", tt.method, tt.path)
		})
	}
}
