package db_test

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/keithwoelke/parking-police/internal/analysis"
	"gitlab.com/keithwoelke/parking-police/internal/db"
	"gitlab.com/keithwoelke/parking-police/internal/db/fake"
)

func TestClient_WriteSighting(t *testing.T) {
	client := &db.Client{DatastoreClient: fake.New()}

	key, err := client.WriteSighting(context.Background(), time.Now(), analysis.Vehicle{})
	require.NoError(t, err)
	require.NotNil(t, key)
	assert.NotEmpty(t, key)
}
