package db

import (
	"context"

	"cloud.google.com/go/datastore"
	"github.com/googleapis/google-cloud-go-testing/datastore/dsiface"
	"github.com/pkg/errors"
)

type Client struct {
	DatastoreClient dsiface.Client
}

func New(ctx context.Context) (dbClient *Client, err error) {
	var datastoreClient *datastore.Client
	if datastoreClient, err = datastore.NewClient(ctx, "parking-police"); err != nil {
		return dbClient, errors.WithStack(err)
	}

	c := &Client{
		DatastoreClient: dsiface.AdaptClient(datastoreClient),
	}

	return c, nil
}
