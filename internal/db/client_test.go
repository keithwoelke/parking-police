package db_test

import (
	"context"
	"testing"

	"cloud.google.com/go/datastore"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/keithwoelke/parking-police/internal/db/fake"
)

func TestClient_Put(t *testing.T) {
	client := fake.New()
	require.NotNil(t, client)

	expectedKey := &datastore.Key{ID: int64(1234), Kind: "kind"}
	expectedValue := struct{}{}
	actualKey, err := client.Put(context.Background(), expectedKey, &expectedValue)
	require.NoError(t, err)
	require.NotNil(t, actualKey)
	assert.Equal(t, expectedKey.ID, actualKey.ID)

	var actualValue struct{}
	err = client.Get(context.Background(), actualKey, &actualValue)
	require.NoError(t, err)
	assert.Equal(t, expectedValue, actualValue)
}

func TestClient_Delete(t *testing.T) {
	client := fake.New()
	require.NotNil(t, client)

	expectedKey := &datastore.Key{ID: int64(1234), Kind: "kind"}
	expectedValue := struct{}{}
	actualKey, err := client.Put(context.Background(), expectedKey, &expectedValue)
	require.NoError(t, err)
	require.NotNil(t, actualKey)
	assert.Equal(t, expectedKey.ID, actualKey.ID)

	err = client.Delete(context.Background(), actualKey)
	require.NoError(t, err)

	var actualValue struct{}
	err = client.Get(context.Background(), actualKey, &actualValue)
	require.Error(t, err)
	assert.Equal(t, datastore.ErrNoSuchEntity, err)
}
