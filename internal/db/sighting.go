package db

import (
	"context"
	"time"

	"cloud.google.com/go/datastore"
	"github.com/pkg/errors"
	"gitlab.com/keithwoelke/parking-police/internal/analysis"
)

// A Sighting is a point in time in which a specific vehicle was observed.
type sighting struct {
	Date        string `json:"date,omitempty"`
	PlateRegion string `json:"region,omitempty"`
	PlateNumber string `json:"number,omitempty"`
	Image       []byte `json:"image,omitempty" datastore:",noindex"`
}

func (c *Client) WriteSighting(ctx context.Context, date time.Time, vehicle analysis.Vehicle) (key *datastore.Key, err error) {
	sightingKey := datastore.IncompleteKey("Sighting", nil)

	dbSighting := sighting{
		Date:        date.Format(time.RFC3339),
		PlateRegion: vehicle.Plate.Region,
		PlateNumber: vehicle.Plate.Number,
		Image:       vehicle.Plate.Image,
	}

	key, err = c.DatastoreClient.Put(ctx, sightingKey, &dbSighting)

	return key, errors.WithStack(err)
}
