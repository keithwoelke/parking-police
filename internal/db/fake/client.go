package fake

import (
	"context"
	"encoding/json"
	"math/rand"
	"reflect"

	"cloud.google.com/go/datastore"
	"github.com/googleapis/google-cloud-go-testing/datastore/dsiface"
)

type Client struct {
	dsiface.Client
	M map[string]interface{}
}

func New() dsiface.Client {
	return &Client{
		M: make(map[string]interface{}),
	}
}

func (c *Client) Put(ctx context.Context, key *datastore.Key, src interface{}) (*datastore.Key, error) {
	if !(reflect.TypeOf(src).Kind().String() == "ptr" && reflect.TypeOf(src).Elem().Kind().String() == "struct") {
		return nil, datastore.ErrInvalidEntityType
	}

	if key.Name == "" && key.ID == 0 {
		key.ID = rand.Int63() //nolint:gosec
	}

	c.M[key.String()] = reflect.ValueOf(src).Interface()

	return key, nil
}

func (c *Client) Get(ctx context.Context, key *datastore.Key, dst interface{}) error {
	valPtr, ok := c.M[key.String()]
	if !ok {
		return datastore.ErrNoSuchEntity
	}

	valBytes, err := json.Marshal(valPtr)
	if err != nil {
		return datastore.ErrInvalidEntityType
	}

	dstType := reflect.ValueOf(dst).Elem().Type()

	convertedVal := reflect.New(dstType).Interface()
	if err = json.Unmarshal(valBytes, &convertedVal); err != nil {
		return datastore.ErrInvalidEntityType
	}

	val := reflect.ValueOf(convertedVal).Elem()
	reflect.ValueOf(dst).Elem().Set(val)

	return nil
}

func (c *Client) Delete(ctx context.Context, key *datastore.Key) error {
	delete(c.M, key.String())
	return nil
}
