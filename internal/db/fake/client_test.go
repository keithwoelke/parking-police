package fake_test

import (
	"context"
	"testing"

	"cloud.google.com/go/datastore"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/keithwoelke/parking-police/internal/db/fake"
)

const expectedKeyKind = "kind"

func TestClient_Get(t *testing.T) {
	fakeClient := fake.New()

	expectedValue := struct{}{}

	key := datastore.IDKey("kind", 12345, nil)
	fakeClient.(*fake.Client).M[key.String()] = expectedValue

	var actualValue struct{}
	err := fakeClient.Get(context.Background(), key, &actualValue)
	require.NoError(t, err)

	assert.Equal(t, expectedValue, actualValue)
}

func TestClient_GetNonExisting(t *testing.T) {
	fakeClient := fake.New()

	key := datastore.IDKey("kind", 12345, nil)

	var actualValue string
	err := fakeClient.Get(context.Background(), key, &actualValue)
	require.Error(t, err)

	assert.Equal(t, datastore.ErrNoSuchEntity, err)
}

func TestClient_GetIncorrectType(t *testing.T) {
	fakeClient := fake.New()

	expectedValue := struct{}{}

	key := datastore.IDKey("kind", 12345, nil)
	fakeClient.(*fake.Client).M[key.String()] = expectedValue

	var actualValue int
	err := fakeClient.Get(context.Background(), key, &actualValue)
	require.Error(t, err)

	assert.Equal(t, datastore.ErrInvalidEntityType, err)
}

func TestClient_GetUnmarshalableType(t *testing.T) {
	fakeClient := fake.New()

	key := datastore.IDKey("kind", 12345, nil)
	fakeClient.(*fake.Client).M[key.String()] = func() {}

	var actualValue func()
	err := fakeClient.Get(context.Background(), key, &actualValue)
	require.Error(t, err)

	assert.Equal(t, datastore.ErrInvalidEntityType, err)
}

func TestClient_PutIDKey(t *testing.T) {
	fakeClient := fake.New()

	expectedValue := struct{}{}
	expectedKeyID := int64(12345)

	keyToCreate := datastore.IDKey(expectedKeyKind, expectedKeyID, nil)
	createdKey, err := fakeClient.Put(context.Background(), keyToCreate, &expectedValue)
	require.NoError(t, err)

	assert.Equal(t, expectedKeyID, createdKey.ID)
	assert.Equal(t, expectedKeyKind, createdKey.Kind)

	actualValue := fakeClient.(*fake.Client).M[createdKey.String()]
	assert.Equal(t, &expectedValue, actualValue)
}

func TestClient_PutIncompleteKey(t *testing.T) {
	fakeClient := fake.New()

	expectedValue := struct{}{}
	expectedKeyKind := "kind"

	keyToCreate := datastore.IncompleteKey(expectedKeyKind, nil)
	createdKey, err := fakeClient.Put(context.Background(), keyToCreate, &expectedValue)
	require.NoError(t, err)
	assert.NotEqual(t, 0, createdKey.ID)
	assert.Equal(t, expectedKeyKind, createdKey.Kind)

	actualValue := fakeClient.(*fake.Client).M[createdKey.String()]
	assert.Equal(t, &expectedValue, actualValue)
}

func TestClient_PutNonStructPointer(t *testing.T) {
	fakeClient := fake.New()

	expectedValue := struct{}{}
	expectedKeyKind := "kind"

	keyToCreate := datastore.IncompleteKey(expectedKeyKind, nil)
	_, err := fakeClient.Put(context.Background(), keyToCreate, expectedValue)
	require.Error(t, err)

	assert.Equal(t, datastore.ErrInvalidEntityType, err)
}

func TestClient_Delete(t *testing.T) {
	fakeClient := fake.New()

	expectedValue := struct{}{}

	key := datastore.IDKey("kind", 12345, nil)
	fakeClient.(*fake.Client).M[key.String()] = expectedValue

	err := fakeClient.Delete(context.Background(), key)
	require.NoError(t, err)

	assert.Equal(t, 0, len(fakeClient.(*fake.Client).M))
}
