#!/usr/bin/env sh

set -e

[ -x "$(command -v git)" ] || { echo "Git is not installed." ; exit 1; }

project_dir="$(cd "${0%/*}/.." && pwd -P)"

git config core.hooksPath "${project_dir}/githooks"
