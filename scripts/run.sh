#!/usr/bin/env sh

set -e

[ -x "$(command -v docker)" ] || { echo "Docker is not installed." ; exit 1; }

GOLANG="golang:1.16-stretch"

: "${GOOGLE_CREDENTIALS?"GOOGLE_CREDENTIALS must be set."}"
: "${OALPR_SECRET_KEY?"OALPR_SECRET_KEY must be set."}"
: "${API_KEY?"API_KEY must be set."}"

DEPLOYMENT_DATE="$(date -u +"%Y-%m-%dT%H:%M:%SZ")"
BUILD_NUMBER="local"

project_dir="$(cd "${0%/*}/.." && pwd -P)"
DOCKER_PROJECT_DIR="/mnt"
DOCKER_GOPATH="/go"

# shellcheck disable=SC2016
docker_command="\
    docker run --rm \
    --mount src=\"${project_dir}\",dst=\"${DOCKER_PROJECT_DIR}\",type=bind \
    -w \"${DOCKER_PROJECT_DIR}\" \
    -e BUILD_NUMBER=\"${BUILD_NUMBER}\" \
    -e DEPLOYMENT_DATE=\"${DEPLOYMENT_DATE}\" \
    -e OALPR_SECRET_KEY=\"${OALPR_SECRET_KEY}\" \
    -e GOOGLE_CREDENTIALS=\"$(printf "%s" '${GOOGLE_CREDENTIALS}')\" \
    -e API_KEY=\"${API_KEY}\" \
    -e 8080 \
    -p 8080:8080"

if [ -e "${GOPATH}" ]; then
    docker_command="${docker_command} \
    --mount src=\"${GOPATH}\",dst=\"${DOCKER_GOPATH}\",type=bind \
    -e GOPATH=\"${DOCKER_GOPATH}\""
fi

# shellcheck disable=SC2140
docker_command="${docker_command} \
    \"${GOLANG}\" bash -c ' \
    credentials_file=\"\$(mktemp)\"; \
    printf "%s" \"\${GOOGLE_CREDENTIALS}\" > \"\${credentials_file}\"; \
    export GOOGLE_APPLICATION_CREDENTIALS=\"\${credentials_file}\"; \
    go build -o /tmp/parking-police; \
    exec /tmp/parking-police'"

eval "${docker_command}"
