#!/usr/bin/env sh

set -e

[ -x "$(command -v docker)" ] || { echo "Docker is not installed."; exit 1; }

ENVSUBST="registry.gitlab.com/keithwoelke/envsubst:63169375"

: "${OALPR_SECRET_KEY?"OALPR_SECRET_KEY must be set."}"
: "${BUILD_NUMBER?"BUILD_NUMBER must be set."}"
: "${API_KEY?"API_KEY must be set."}"

DEPLOYMENT_DATE="$(date -u +"%Y-%m-%dT%H:%M:%SZ")"

project_dir="$(cd "${0%/*}/.." && pwd -P)"
DOCKER_PROJECT_DIR="/mnt"

echo "Generating app.yaml"
docker run --rm \
    --mount src="${project_dir}",dst="${DOCKER_PROJECT_DIR}",type=bind \
    -w "${DOCKER_PROJECT_DIR}" \
    -e BUILD_NUMBER="${BUILD_NUMBER}" \
    -e OALPR_SECRET_KEY="${OALPR_SECRET_KEY}" \
    -e DEPLOYMENT_DATE="${DEPLOYMENT_DATE}" \
    -e API_KEY="${API_KEY}" \
    "${ENVSUBST}" sh -c '
    envsubst < configs/app.yaml > cmd/sighting-sorter/app.yaml'
