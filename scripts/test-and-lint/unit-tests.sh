#!/usr/bin/env sh

set -e

[ -x "$(command -v docker)" ] || { echo "Docker is not installed."; exit 1; }

GOLANG="golang:1.16-stretch"

project_dir="$(cd "${0%/*}/../.." && pwd -P)"
DOCKER_PROJECT_DIR="/mnt"

echo "Running unit tests"

docker_command="\
    docker run --rm \
    --mount src=\"${project_dir}\",dst=\"${DOCKER_PROJECT_DIR}\",type=bind \
    -w \"${DOCKER_PROJECT_DIR}\""

docker_command="${docker_command} \
    \"${GOLANG}\" sh -c ' \
    go test -race -count 1 -coverpkg ./... -coverprofile coverage.out ./...'"

eval "${docker_command}"
