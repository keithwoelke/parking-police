#!/usr/bin/env sh

set -e

[ -x "$(command -v docker)" ] || { echo "Docker is not installed."; exit 1; }

GOLANG="golang:1.16-stretch"

project_dir="$(cd "${0%/*}/../.." && pwd -P)"
DOCKER_PROJECT_DIR="/mnt"
DOCKER_GOPATH="/go"

echo "Generating code coverage report"

docker_command="\
    docker run --rm \
    --mount src=\"${project_dir}\",dst=\"${DOCKER_PROJECT_DIR}\",type=bind \
    -w \"${DOCKER_PROJECT_DIR}\""

if [ -e "${GOPATH}" ]; then
    docker_command="${docker_command} \
    --mount src=\"${GOPATH}\",dst=\"${DOCKER_GOPATH}\",type=bind \
    -e GOPATH=\"${DOCKER_GOPATH}\""
fi

docker_command="${docker_command} \
    \"${GOLANG}\" sh -c ' \
    go tool cover -func coverage.out
    go tool cover -html coverage.out -o coverage.html'"

eval "${docker_command}"
