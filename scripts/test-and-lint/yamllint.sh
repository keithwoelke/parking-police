#!/usr/bin/env sh

set -e

[ -x "$(command -v docker)" ] || { echo "Docker is not installed."; exit 1; }

NODE_CHECKS="registry.gitlab.com/keithwoelke/node-checks:523262351"

project_dir="$(cd "${0%/*}/../.." && pwd -P)"
DOCKER_PROJECT_DIR="/mnt"

echo "Running jsonlint"
docker run --rm \
    --mount src="${project_dir}",dst="${DOCKER_PROJECT_DIR}",type=bind \
    -w "${DOCKER_PROJECT_DIR}" \
    "${NODE_CHECKS}" sh -c '
    find . -name "*\\.json" -print0 | xargs -0 -n 1 jsonlint --quiet'
