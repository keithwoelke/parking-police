#!/usr/bin/env sh

set -e

[ -x "$(command -v docker)" ] || { echo "Docker is not installed."; exit 1; }

GOLANGCI_LINT="golangci/golangci-lint:v1.45.2"

project_dir="$(cd "${0%/*}/../.." && pwd -P)"
DOCKER_PROJECT_DIR="/mnt"

echo "Running \"all\" golangci-lint checks"

docker_command="\
    docker run --rm \
    --mount src=\"${project_dir}\",dst=\"${DOCKER_PROJECT_DIR}\",type=bind \
    -w \"${DOCKER_PROJECT_DIR}\""

docker_command="${docker_command} \
    \"${GOLANGCI_LINT}\" sh -c ' \
    golangci-lint run --skip-dirs-use-default --config configs/golangci-lint/full.yaml ./...'"

eval "${docker_command}"
