#!/usr/bin/env sh

set -e

[ -x "$(command -v docker)" ] || { echo "Docker is not installed." ; exit 1; }

: "${GOOGLE_CREDENTIALS?"GOOGLE_CREDENTIALS must be set."}"
: "${PROJECT_NAME?"PROJECT_NAME must be set."}"

GCLOUD="registry.gitlab.com/keithwoelke/gcloud:523276366"

project_dir="$(cd "${0%/*}/.." && pwd -P)"
DOCKER_PROJECT_DIR="/mnt"

echo "Deploying to App Engine"
docker run --rm \
    --mount src="${project_dir}",dst="${DOCKER_PROJECT_DIR}",type=bind \
    -w "${DOCKER_PROJECT_DIR}" \
    -e GOOGLE_CREDENTIALS="${GOOGLE_CREDENTIALS}" \
    -e CLOUDSDK_CORE_PROJECT="${PROJECT_NAME}" \
    "${GCLOUD}" sh -c '
    credentials_file="$(mktemp)"
    printf "%s" "${GOOGLE_CREDENTIALS}" > "${credentials_file}"
    gcloud auth activate-service-account app-engine-deployer@parking-police.iam.gserviceaccount.com --key-file "${credentials_file}"
    gcloud app deploy cmd/sighting-sorter/app.yaml'
