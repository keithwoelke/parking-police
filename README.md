# parking-police

[![pipeline status](https://gitlab.com/keithwoelke/parking-police/badges/master/pipeline.svg)](https://gitlab.com/keithwoelke/parking-police/commits/master)
[![coverage report](https://gitlab.com/keithwoelke/parking-police/badges/master/coverage.svg)](https://gitlab.com/keithwoelke/parking-police/commits/master)

Library to parse license plates and collect metrics.
